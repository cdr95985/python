k = 2
n = 10

# iterates till x <= n - 1
print("For loop with incrementing by k: -------------------")
for x in range(1,n,k):
  print(x)
  x = 100  # x is set equal to 100; i.e.) x is initialized at every iteration rather than at the start of the loop
  print(x)

print("Nested for loop, depending on i: -------------------")
for i in range(n):
    for x in range(i):
        # Statement(s) that take(s) constant time
        print(x)

print("While loop, multiples/ divides the loop counter: -------------------")
i = 1 #constant
n = 16 #constant
k = 2 #constant
while i < n:
    print(i)
    i *= k
    # Statement(s) that take(s) constant time