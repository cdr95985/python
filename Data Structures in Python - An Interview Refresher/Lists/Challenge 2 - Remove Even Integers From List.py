# this is bad because it does not filter out unique values, and uses built in .sort()
def mergeArrays(lst1, lst2):
    res = []

    for i in lst1:
        res.append(i)

    for i in lst2:
        res.append(i)

    res.sort()

    return res

# this takes TWO SORTED ARRAYS, and then merges them together in order
# Runtime: O(n + m)
def mergeArrays1(lst1, lst2):
    result = []
    index_arr1 = index_arr2 = index_result = 0

    # fill result list as long as both lists summed together
    for i in range(len(lst1) + len(lst2)):
        result.append(i)

    # now get traverse both lists and insert the smaller value into the reuslt list
    while ( index_arr1 < len(lst1)) and (index_arr2 < len(lst2)):
        # if item in lst1 is less than item in lst2
        if(lst1[index_arr1] < lst2[index_arr2]):
            # add it to result
            result[index_result] = lst1[index_arr1]
            # increment lst1 and result counters
            index_arr1 += 1
            index_result += 1
        # other wise, the item in lst2 is less than item in lst1
        else:
            # add it to the result
            result[index_result] = lst2[index_arr2]
            index_arr2 += 1
            index_result += 1

    # if one of the lists is completely traversed, and the other is not, then just copy the remaining elements
    # into the result list
    while (index_arr1 < len(lst1)):
        result[index_result] = lst1[index_arr1]
        index_result +=1
        index_arr1 +=1
    while (index_arr2 < len(lst2)):
        result[index_result] = lst2[index_arr2]
        index_result +=1
        index_arr2 +=1
    return result

# merge two sorted arrays by editing one of the arrays in place; slower, but takes less space
# time: O(m(n+m)) space: O(m)
def mergeArrays2(lst1, lst2):
    ind1 = ind2 = 0

    while(ind1 < len(lst1) and ind2 < len(lst2)):
        # if item in list 1 is greater than item in list 2
        if(lst1[ind1] > lst2[ind2]):
            lst1.insert(ind1, lst2[ind2]) # insert list2 item before list1 item
            ind1 += 1
            ind2 += 1
        else:
            ind1 += 1

    # if anything is left in list 2, just append it
    if(ind2 < len(lst2)):
        lst1.extend(lst2[ind2:])
        
    return lst1

print(mergeArrays([1,3,4,5], [2,6,7,8]))
print(mergeArrays1([1,3,4,5], [2,6,7,8]))
print(mergeArrays1([4,5,6],[-2,-1,0]))
print(mergeArrays2([1,3,4,5], [2,6,7,8]))
print(mergeArrays2([4,5,6],[-2,-1,0]))