# via list comprehension: get each item in the list that is odd and return it in a new list
def removeEven1(List):
    return [item for item in List if item%2 !=0]

# return a new list containing only the odd values
def removeEven2(List):
    new = []
    for item in List:
        if item % 2 != 0:
            new.append(item)
    return new

# if mutating the list by manually removing even elements, we must do this in reverse order, 
# as the iterator will "lose track" of the indexes as the algorithm progresses
def removeEven3(List):
    length = len(List)
    for i in range(length, -1, -1):
        item = List[i - 1]
        if(item % 2 == 0):
            List.remove(item)

    return List

print(removeEven1([1,2,4,5,10,6,3]))
print(removeEven2([1,2,4,5,10,6,3]))
print(removeEven3([1,2,4,5,10,6,3]))