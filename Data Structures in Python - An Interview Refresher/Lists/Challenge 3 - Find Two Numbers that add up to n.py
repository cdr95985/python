'''
Take a number n as an input, and return two numbers that add up to n

Time: O(n^2)
Space: O(n)?
'''
def findSumBruteForce(lst, value):
    res = []
    # for each item in list    
    for i in lst:
        # compare this value against each other item in the list
        for j in lst:
            # if they sum to given value
            if (i + j) == value:
                # return it
                return [i, j]

    return res

'''
Sorting the list first; then, for each of the given elements,
see if its complement (T - n) is in the array, if so, get its index
so we can return its value.

Uses binarySearch() and findSumSorted()

Time: O(n log n) ; assuming python.sort() is O(n log n)
Space: O(n)?
'''

def binarySearch(a, item):
    first = 0
    last = len(a) - 1
    found = False
    while first <= last and not found:
        mid = (first + last)
        if a[mid] == item:
            found = mid
        else:
            if item < a[mid]:
                last = mid - 1
            else:
                first = mid + 1
    return found

def findSumSorted(lst, value):
    lst.sort()
    for j in lst:
        index = binarySearch(lst, value - j)
        if index: return [j, value - j]


'''
Takes two indices starting at the beginning and end, and compares their sum
to the given value.

Time: O(n log n)
'''
def findSumMovingIndices(lst, value):
    lst.sort()
    index1 = 0
    index2 = len(lst) - 1
    result = []
    sum = 0
    while index1 != index2:
        sum = lst[index1] + lst[index2]
        if sum < value:
            index1 += 1
        elif sum > value:
            index2 -= 1
        else:
            result.append(lst[index1])
            result.append(lst[index2])
            return result
    return False


'''
Check to see if the complement of the value and this value exist in
the dictionary, otherwise set the key to false (0)

Time: O(n)
'''
def findSumWithDictionary(lst, value):
    foundValues = {}
    for ele in lst:
        try:
            foundValues[value - ele]
            return [value - ele, ele]
        except:
            foundValues[ele] = 0
    return False

'''
Does the same thing as using a dictinary without using try/except, using python's set()

Time: O(n)
'''
def findSumWithSet(lst, value):
    # A Set is an unordered collection data type that is iterable, mutable, and has no duplicate elements
    res = set()

    # for each item
    for ele in lst:
        # if the given value minus this item is in our set
        # note: calculating the difference between value and ele allows and storing the result
        # allows us to stop if that element exists in our set if we happen to find a new element
        # whose difference between it and the value match one of the previously stored elements
        if value - ele in res:
            # return these two numbers
            return [value - ele, ele]
        # otherwise, add this item to our set
        res.add(ele)

    return false

#print(findSumSorted([1,2,3,4], 5))
print(findSumMovingIndices([1,2,3,4], 5))
print(findSumWithDictionary([1,2,3,4], 5))