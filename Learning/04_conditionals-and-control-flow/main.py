import sys

print("--- start if conditions")
try:

    #x = int(input("Please enter an integer: "))
    x = 5

    if x == 0:
        print("x is 0")
    elif x > 5:
        print("x > 5")
    elif x < 5:
        print("x < 5")
    else:
        print("x is 5")

    if isinstance(x, int):
        print("x is int")
    else:
        print("x is NOT int")
except ValueError:
    print("You did not enter a valid number!")
except:
    # use sys.exc_info() to get last error
    print("Unhandled error occured", sys.exc_info()[0])

print("--- start for loops")
# lists are 0-indexed
list = ['alpha', 'beta', 'gamma']
for l in list:
    print(l, len(l))

print("--- start while loops")
i = 0
while(True):
    if(i > len(list) - 1): break
    if i == 1:
        print("Skipping item 1")
        i += 1
        continue
    print(i, list[i])
    # increment/decrement operators do not exist in python
    i += 1

print("--- start range()")
for i in range(len(list)):
    if i == 2:
        print("Skipping item 2")
        continue

    print(i, list[i])