from functools import reduce

# A function that shows the results of running competitions consisting of 2 to 4 runners.
def save_ranking(first, second, third=None, fourth=None):
    rank = {}
    rank[1], rank[2] = first, second
    rank[3] = third if third is not None else 'Nobody'
    rank[4] = fourth if fourth is not None else 'Nobody'
    print(rank)


# Pass the 2 positional arguments
print("--- start: keyword arguments")
save_ranking('ming', 'alice')
# Pass the 2 positional arguments and 1 keyword argument
save_ranking('alice', 'ming', third='mike')
# Pass the 2 positional arguments and 2 keyword arguments (But, one of them was passed as like positional argument)
save_ranking('alice', 'ming', 'mike', fourth='jim')
save_ranking('alice', 'ming', fourth='jim')


def save_ranking2(*args):
    print(args)


print("--- start positional arguments")
save_ranking('ming', 'alice', 'tom', 'mike')


def save_ranking3(**kwargs):
    print(kwargs)


print("--- start keyword arguments (variadic)")
save_ranking3(first='ming', second='alice', third='tom', fourth='mike')


def save_ranking4(*args, **kwargs):
    print(args)
    print(kwargs)


print("--- start keyword arguments (variadic)")
save_ranking4('ming', 'tom', second='alice', fourth='mike')
# NOTE: positional arguments must come before keyword arguments; the below does not work!
# save_ranking4('ming', second='alice', 'tom', fourth='mike')


def product(*numbers):
    p = reduce(lambda x, y: x * y, numbers)
    return p


primes = [2, 3, 5, 7, 11, 13]
print("--- start list un/packing example:", product(primes))
print("Product of primes w/o unpacking:", product(primes))
print("Product of primes WITH unpacking:", product(*primes))


# IMPORTANT: positional parameters must precede keyword parameters!
# def pre_process(**headers, temp): # does not work
def pre_process(temp, **headers):
    content_length = headers['Content-Length']
    print('content-length', content_length)

    host = headers['Host']
    if 'https' not in host:
        raise ValueError('You must use SSL for http communication')


headers = {
    'Accept': 'text/plain',
    'Content-Length': 348,
    #'Host': 'http://google.com'
    'Host': 'https://google.com'
}
print("--- start tuple un/packing example:", product(primes))
pre_process("test", **headers)
# IMPORTANT: positional arguments must precede keyword arguments!
# pre_process("test", **headers, "test") # does not work
