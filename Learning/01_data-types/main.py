'''
Multi-line comment in python

See: https://realpython.com/python-data-types/
'''
# integer has no length (aside from memory constraints)
# a sequence of decimal digits without any prefix will become a decimal number
print(1232111111111111111111111111111111111111111111111111111111111111111111111111111111)
# print decimal is binary
print(0b10, type(0b10))
# print decimal is octal
print(0o10, type(0o10))
# print decimal is hex
print(0x10, type(0x10))
# print floating point
print(4.2, type(4.2))
# print floating point with sci notation
print(4.2e-4, type(4.2e-4))
# complex numbers are a thing
print(2+3j, type(2+3j))
# assign value to multiple variables
a, b, c = "alpha", "beta", "gamma"
print(a, b, c, type(a))
# a[0] = "BB" # strings are immutable
print("print a string after modifying it", a, b, c, type(a))
print("first 3 chars of a", a[0:3])
print("first 3 chars of a", a[:3])
print("last 3 chars of a", a[-3:])
print("a stats with 'a'", a.startswith('a'))
print("a ends with 'a'", a.endswith('a'))
print("String \"Lite\\rals\" done like so; \tThis should be Tabbed")


# functions must be declared upstream prior to invoking them, this will not work:
#alpha(True)


# declare function; functions should have 2 blank lines before and after them
def alpha(a,b=True):
    """This is function documentation for alpha()"""
    print("Alpha() envoked:", a, b)
# print function docs
print(alpha.__doc__)
# use default value for b
alpha(True)
# override value for b
alpha(False, False)
l = (lambda: print("Invoked lambda function"))
l()
l = (lambda a,b: a+b)
print("invoking lambda function with arguments", l(5, 10))


def callback1(a):
    print("Invoked callback 1:", a)


def callback2(a):
    print("Invoked callback 2:", a)


def function(a,cb):
    a + 1
    print("I am function, and am invoking given callback as: cb(a)", a)
    cb(a)


function(5, callback1)
function(500, callback2)
list = [1, 2, 3, 4, 5]
print("print a list", list)
list[0] = 100
print("print a list after modifying it", list)