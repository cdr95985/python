# importing by relative path; this is not the standard, see: https://stackoverflow.com/questions/16981921/relative-imports-in-python-3/28154841
from linked_list import LinkedList

ll = LinkedList()
ll.insert(1)
ll.insert(2)
ll.insert(3)
ll.insert(4)

# invoke public method
ll.print()

print("public memember: head", ll.head)
# cannot access protected members
#print(ll._protected)
# cannot invoke private members
#print(ll.__private)
#ll.__print()
