# importing by relative path; this is not the standard, see: https://stackoverflow.com/questions/16981921/relative-imports-in-python-3/28154841
from node import Node

class LinkedList(object):
    def __init__(self, head=None):
        self.head = head
        self._protected = "a protected attribute; accessible by sub-classes"
        self.__private = "a private attribute; accessible by this instance only"

    # "private" method
    def __print(self):
        curr = self.head
        while curr:
            print(curr.data)
            curr = curr.get_next()

    def insert(self, data):
        new_node = Node(data)
        new_node.set_next(self.head)
        self.head = new_node

    # public method
    def print(self):
        self.__print()

