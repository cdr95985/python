'''
Python is 'pass-by-object-value, which is different from '02_pass-by-value' and 'pass-by-reference'

A variable contains a value.

Pass-by-reference;
    - Passing a variable into a function and manipulating it will affect the variable in both the scope of the function,
    and the scope of its caller; in other words, when said variable is changed in either place, the exact same address
    in memory is updated

    # NOTE: the below is not how python behaves, and is strictly for demonstration purposes
    def func(list):
        # manipulate list
        list.append[1] # list = [0,1]


    list = [0]
    print("List before:", list) # list = [0]
    append(list)
    print("List after:", list) # list = [0]


Pass-by-value:
    - A variable passed into a function will be cloned into another place in memory, and referenced only in this function.
    If the caller will be unable to access that variable itself.

    # NOTE: the below is not how python behaves, and is strictly for demonstration purposes
    def func(list):
        # manipulate list
        list.append[1] # list = [0,1]


    list = [0]
    print("List before:", list) # list = [0]
    append(list)
    print("List after:", list) # list = [0, 1]


Pass-by-object-value:
    - Object references are passed in by value
    - A variable passed into a function will reference the same place in memory, and will have a different variable
    referencing the same place in memory
    - The function and its caller use the same object in memory, but under different names!
    - On the surface, the example below LOOKS like Pass-by-reference, but it is actually not! The variable "list"
    in the caller scope and in the function scope are "DIFFERENT" variables (even though they are the same name), but
    they reference the same value in memory

    # NOTE: this IS how python behaves
    def func(list):
        # manipulate list
        list.append[1] # list = [0,1]


    list = [0]
    print("List before:", list) # list = [0]
    append(list)
    print("List after:", list) # list = [0, 1]
'''

def append(list):
    list.append(1)


list = [0]
print("List before:", list)
append(list)
print("List after:", list)

'''
NOTE: The pass-by-object-reference does NOT apply to NON-OBJECTS!
'''

def func(y):
    print("Given y:", y)
    # manipulate a
    y += 5
    print("Func changed y to:", y)


# init a
a = 0
print("A before:", a)
func(a)
print("A after:", a)