class Solution:
    def reverse(self, string):
        if len(string) == 0:
            return string
        else:
            return self.reverse(string[1:]) + string[0]

    def reverseIterative(self, string):
        answer = ''
        stack = [string]
        while len(stack):
            item = stack.pop()
            answer += item[-1]

            nextItem = item[:-1]
            if len(nextItem):
                stack.append(nextItem)
        return answer


def reverseStringIterative(str):
    i = len(str) - 1
    res = ""
    # get length of string
    while i >= 0:
        res += str[i]
        i -= 1
    return res


def recurseStringRecursive(string):
    if len(string) == 0:
        return string
    else:
        return recurseStringRecursive(string[1:]) + string[0]


print("Reverse string recursively: ")
print(recurseStringRecursive("alpha"))
print("Reverse string iteratively: ")
print(reverseStringIterative("alpha"))
print("Reverse string recursively OOP: ")
print(Solution().reverse("beta"))
print("Reverse string iteratively OOP: ")
print(Solution().reverseIterative("beta"))
