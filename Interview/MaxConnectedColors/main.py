class Solution:
    def maxConnected(self, grid):
        maxCount = 0
        for y in range(0, len(grid)):
            for x in range(0, len(grid[y])):
                count = self.__traverse(grid, x, y, {})
                maxCount = max(count, maxCount)

        return maxCount

    def __traverse(self, grid, x, y, seen):
        return self.__traverseHelper(grid, x, y, 0, seen)

    def __traverseHelper(self, grid, x, y, count, seen):
        key = "%s,%s" % (x, y)

        if seen.get(key) != None:
            return 0
        seen[key] = True

        color = grid[y][x]
        neighbors = self.__getNeighbors(grid, x, y)
        sum = 1
        for point in neighbors:
            i = point[0]
            j = point[1]
            if grid[j][i] != color:
                continue
            seen[key] = True
            sum += self.__traverseHelper(grid, i, j, 1, seen)
        return sum

    def __getNeighbors(self, grid, x, y):
        coords = []
        neighbors = [
            [-1, 0],
            [0, -1],
            [1, 0],
            [0, 1]
        ]
        for n in neighbors:
            coordX = x + n[0]
            coordY = y + n[1]
            if(coordX < 0 or coordY < 0 or coordY >= len(grid) or coordX >= len(grid[0])):
                continue
            coords.append([coordX, coordY])
        return coords

grid = [
    ['r', 'g', 'g', 'g', 'g', 'b'],
    ['r', 'r', 'r', 'g', 'g', 'b'],
    ['g', 'g', 'b', 'b', 'b', 'b']
]


print(Solution().maxConnected(grid))
