# see: "C:\Users\cdr95985.AD\Google Drive\resume\TechSeries Resources\Worksheet_Linked_Lists.pdf"
class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


class LinkedList:
    head = None

    def add(self, node):
        if self.head is None:
            self.head = node
        else:
            currNode = self.head
            while currNode.next is not None:
                currNode = currNode.next
            currNode.next = node
            node.next = None

    def print(self, node):
        while node is not None:
            print(node.value, node)
            node = node.next

    # iteratively
    def reverse(self):
        curr = self.head
        prev = None

        while curr:
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next

        # return the new head
        self.head = prev
        return prev

    # recurse
    def reverseRecurse(self):
        self.head = self.reverseRecurseHelper(None, self.head)
        return self.head

    def reverseRecurseHelper(self, prev, curr):
        if curr:
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next
            return self.reverseRecurseHelper(prev, curr)
        else:
            return prev

    # O(N) time | O(N) space
    def deleteValue(self, value):
        curr = self.head
        prev = None
        while curr:
            # if the current value matches given value
            if(curr.value == value):
                # and if it has a next node
                if curr.next:
                    curr.value = curr.next.value
                    # point curr.next to the next, next node (thereby removing the intermediary node)
                    curr.next = curr.next.next
                    # set prev
                    prev = curr
                    # increment to next node
                    curr = curr.next

                # and it does not have a next node
                else:
                    # we are at the tail, the previous node needs to be pointed to None
                    prev.next = None
                    curr = None
            else:
                # set prev
                prev = curr
                # increment to next node
                curr = curr.next

    # O(1) time | O(1) space
    def deleteNode(self, node):
        if node.next:
            # WARNING: the following does not work b/c parameter "node" is PASS BY VALUE in python; re-assigning obj has 0 effect in caller's scope
            #temp = node
            #node = Node(node.next.value)
            #node.next = temp.next.next

            node.value = node.next.value
            node.next = node.next.next
        else:
            # WARNING: deleting the tail node won't work in python b/c we can't mutate (re-assign) this node itself outside of the scope!
            # Therefore we need to remove by value instead of node! We could do this if we had a reference to the previous node
            node = None

List = LinkedList()
one = Node(1)
two = Node(2)
three = Node(3)
four = Node(4)
five = Node(5)
six = Node(6)

List.add(one)
List.add(two)
List.add(three)
List.add(four)
List.add(five)
List.add(six)
List.add(Node(4))

print("Print list")
List.print(List.head)
print("Print list after reverse (iterative)")
List.print(List.reverse())
print("Print list after reverse (recursive)")
List.print(List.reverseRecurse())
print("Remove value 4 - using value")
List.deleteValue(4)
print("Print list")
List.print(List.head)
# TRICKY: Because we used .deleteValue(), that method "recycled" node @ address of var named four, if we want to delete
# that node specifically, we need to reference it here
print("Delete node 5 - using node (succeed)")
List.deleteNode(four)
print("Print list")
List.print(List.head)
# TRICKY AGAIN:
print("Delete node 6 - using tail node (fail)")
List.deleteNode(four)
print("Print list")
List.print(List.head)
