# Feel free to add new properties and methods to the class.
class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def setHead(self, node):
        # Write your code here.
        if self.head is None:
            self.head = node
            self.tail = node
            return
        self.insertBefore(self.head, node)

    def setTail(self, node):
        # Write your code here.
        if self.tail is None:
            self.setHead(node)
            return
        self.insertAfter(self.tail, node)

    def insertBefore(self, node, nodeToInsert):
        # Write your code here.
        prev = node.prev
        prev.next = nodeToInsert
        node.prev = nodeToInsert
        nodeToInsert.prev = prev
        nodeToInsert.next = node

    def insertAfter(self, node, nodeToInsert):
        # Write your code here.
        next = node.next
        return

    def insertAtPosition(self, position, nodeToInsert):
        # Write your code here.
        return

    def removeNodesWithValue(self, value):
        # Write your code here.
        return

    def remove(self, node):
        # Write your code here.
        if node.next is not None:

        return

    def containsNodeWithValue(self, value):
        # Write your code here.
        return
